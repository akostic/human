<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Human
 * @package app
 *
 * @property integer id
 * @property string unique_master_citizen_number
 * @property string name
 * @property string gender
 * @property integer height
 * @property float weight
 * @property string hair_color
 * @property string eyes_color
 * @property string blood_type
 * @property string nationality
 */
class Human extends Model
{
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUniqueMasterCitizenNumber()
    {
        return $this->unique_master_citizen_number;
    }

    /**
     * @param string $unique_master_citizen_number
     */
    public function setUniqueMasterCitizenNumber($unique_master_citizen_number)
    {
        $this->unique_master_citizen_number = $unique_master_citizen_number;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return string
     */
    public function getHairColor()
    {
        return $this->hair_color;
    }

    /**
     * @param string $hair_color
     */
    public function setHairColor($hair_color)
    {
        $this->hair_color = $hair_color;
    }

    /**
     * @return string
     */
    public function getEyesColor()
    {
        return $this->eyes_color;
    }

    /**
     * @param string $eyes_color
     */
    public function setEyesColor($eyes_color)
    {
        $this->eyes_color = $eyes_color;
    }

    /**
     * @return string
     */
    public function getBloodType()
    {
        return $this->blood_type;
    }

    /**
     * @param string $blood_type
     */
    public function setBloodType($blood_type)
    {
        $this->blood_type = $blood_type;
    }

    /**
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }
}